import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * User: Tolya
 * Date: 10/28/2014
 */
public class Scissors {
    BufferedImage bufferedImage;
    AverageCalculator averageCalculator;

    public Scissors(BufferedImage bufferedImage, AverageCalculator averageCalculator) {
        this.bufferedImage = bufferedImage;
        this.averageCalculator = averageCalculator;
    }

    public Fragment cutSquare(int x0, int y0, int size) {
        int[] fragments = bufferedImage.getRGB(x0, y0, size, size,
                null, 0, size);
        int average = averageCalculator.getAverage(fragments);
        return new Fragment(average, new Point(x0, y0), size);
    }

}
