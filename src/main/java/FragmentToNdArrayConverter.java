import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

/**
 * User: Tolya
 * Date: 11/18/2014
 */
public class FragmentToNdArrayConverter {
    public INDArray convert(Fragment fragment){
        double[] in = new double[4];
        in[0] = fragment.getRgb();
        in[1] = fragment.getPosition().getX();
        in[2] = fragment.getPosition().getY();
        in[3] = fragment.getSize();
        in[1] = 0;
//        in[2] = 0;
        in[3] = 0;
        return Nd4j.create(in);
    }
}
