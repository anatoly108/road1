import org.apache.commons.io.IOUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import java.io.*;
import java.util.List;

/**
 * User: Tolya
 * Date: 11/20/2014
 */
public class FragmentDataSetFactory {
    private Scissors scissors;
    private FragmentToNdArrayConverter fragmentToNdArrayConverter;

    public FragmentDataSetFactory(Scissors scissors, FragmentToNdArrayConverter fragmentToNdArrayConverter) {
        this.scissors = scissors;
        this.fragmentToNdArrayConverter = fragmentToNdArrayConverter;
    }

    public DataSet create(String filePath) throws IOException {
        File f = new File(filePath);
        InputStream fis = new FileInputStream(f);
        List<String> lines = IOUtils.readLines(fis);
        int fragmentsNum = lines.size();

        INDArray data = Nd4j.ones(fragmentsNum, 4);
        double[][] outcomes = new double[fragmentsNum][2];
        int startFrom = 1;

        for (int i = startFrom; i < fragmentsNum; i++) {
            String line = lines.get(i);
            String[] splitted = line.split(",");
            int x = Integer.parseInt(splitted[0]);
            int y = Integer.parseInt(splitted[1]);
            int prediction = Integer.parseInt(splitted[2]);
            outcomes[i] = new double[]{0.0 + prediction, 1.0 - prediction};
            Fragment fragment = scissors.cutSquare(x, y, Constants.FRAGMENT_SIZE);
            INDArray vector = fragmentToNdArrayConverter.convert(fragment);
            data.putRow(i, vector);
        }

        INDArray outNd = Nd4j.create(outcomes);
        return new DataSet(data, outNd);
    }
}
