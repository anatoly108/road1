import java.awt.*;

/**
 * User: Tolya
 * Date: 10/20/2014
 */
public class Fragment {
    private int rgb;
    private Point position;
    private int size;

    public Fragment(int rgb, Point pos, int size) {
        this.rgb = rgb;
        this.position = pos;
        this.size = size;
    }

    public int getRgb() {
        return rgb;
    }

    public Point getPosition() {
        return position;
    }

    public int getSize() {
        return size;
    }

}
