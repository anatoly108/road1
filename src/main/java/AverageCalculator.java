import java.util.stream.IntStream;

/**
 * User: Tolya
 * Date: 10/28/2014
 */
public class AverageCalculator {
    public int getAverage(int[] fragments){
        int average = IntStream.of(fragments).sum();
        average = average / fragments.length;
        return average;
    }
}
