import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.deeplearning4j.distributions.Distributions;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.classifiers.dbn.DBN;
import org.deeplearning4j.models.featuredetectors.rbm.RBM;
import org.deeplearning4j.nn.WeightInit;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.nd4j.linalg.api.activation.Activations;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * User: Tolya
 * Date: 10/4/2014
 */
public class Main {


    public static void main(String[] args) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("res\\0.bmp"));
        AverageCalculator averageCalculator = new AverageCalculator();
        Scissors scissors = new Scissors(bufferedImage, averageCalculator);
        FragmentToNdArrayConverter fragmentToNdArrayConverter = new FragmentToNdArrayConverter();

        FragmentDataSetFactory fragmentDataSetFactory = new FragmentDataSetFactory(scissors, fragmentToNdArrayConverter);
        DataSet completedData = fragmentDataSetFactory.create("res\\0.csv");

        RandomGenerator gen = new MersenneTwister(10);
        NeuralNetConfiguration conf = new NeuralNetConfiguration.Builder()
                .hiddenUnit(RBM.HiddenUnit.RECTIFIED).momentum(5e-1f)
                .visibleUnit(RBM.VisibleUnit.GAUSSIAN).regularization(true)
                .regularizationCoefficient(2e-4f).dist(Distributions.uniform(gen))
                .activationFunction(Activations.tanh()).iterations(10000)
                .weightInit(WeightInit.DISTRIBUTION)
                .lossFunction(LossFunctions.LossFunction.RECONSTRUCTION_CROSSENTROPY).rng(gen)
                .learningRate(1e-3f).nIn(4).nOut(2).build();

        DBN dbn = new DBN.Builder().configure(conf)
                .hiddenLayerSizes(new int[]{3})
                .build();
        dbn.getOutputLayer().conf().setActivationFunction(Activations.softMaxRows());
        dbn.getOutputLayer().conf().setLossFunction(LossFunctions.LossFunction.MCXENT);

        completedData.normalizeZeroMeanZeroUnitVariance();
        dbn.fit(completedData);

        DataSet dataToPredict = fragmentDataSetFactory.create("res\\0p.csv");

        Evaluation evaluation = new Evaluation();
        INDArray output = dbn.output(dataToPredict.getFeatureMatrix());
        evaluation.eval(dataToPredict.getLabels(), output);
        System.out.println("Score " + evaluation.stats());

        /*
        Учитывать фактуру фрагмента, а не только общий цвет RGB?
         */
    }
}
