import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * User: Tolya
 * Date: 10/28/2014
 */
public class ScissorsTest {
    @Test
    public void testName() throws Exception {
        BufferedImage bufferedImage = ImageIO.read(new File("C:\\projects\\road1\\res\\0.bmp"));
        AverageCalculator averageCalculator = new AverageCalculator();
        Scissors target =
                new Scissors(bufferedImage, averageCalculator);
        Fragment actual = target.cutSquare(0, 0, 5);
        System.out.println(actual);
    }
}
