import org.junit.Assert;
import org.junit.Test;

/**
 * User: Tolya
 * Date: 10/28/2014
 */
public class AverageCalculatorTests {
    @org.junit.Test
    public void testName() throws Exception {
        AverageCalculator target = new AverageCalculator();
        int[] fragments = new int[]{1, 2, 3, 4, 5};
        int actual = target.getAverage(fragments);
        int expected = 3;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testName2() throws Exception {
        AverageCalculator target = new AverageCalculator();
        int[] fragments = new int[]{1, 2, 3, 4};
        int actual = target.getAverage(fragments);
        int expected = 2;
        Assert.assertEquals(expected, actual);
    }
}
